<?php require_once 'header.php'; ?>

<section class="subpageHeader">
    <div class="pattern"></div>
    <div class="container">
        <div class="col-md-12 textSliders">
            <h1 class="text-uppercase text-center">Contact</h1>
        </div>
    </div>
</section>

<?php

    $captcha = new \Captcha\Captcha();
    $captcha->setPublicKey('6LfW5h8TAAAAAMAl1A5uDq4XD3oU3I7M5qlrFNk9');
    $captcha->setPrivateKey('6LfW5h8TAAAAAFxPLoAj4epT0CmaNi_L8cjbbl6S');

    $request = array_map(function ($item) {
        return trim(filter_var($item, FILTER_SANITIZE_STRING));
    }, $_POST);

    $message = '';
    $error = false;

    if (isset($request['contact'])) {
        $captcha->setRemoteIp($_SERVER['REMOTE_ADDR']);
        $response = $captcha->check();

        if ($response->isValid()) {

            $phpMailer = PHPMailerConnection();

            /** @var PHPMailer $connection */
            $connection = $phpMailer['connection'];

            $template = $phpMailer['template'];

            $variables = [
                '%name%' => $request['name'],
                '%subject%' => $request['subject'],
                '%mail%' => $request['mail'],
                '%message%' => $request['content'],
                '%type%' => $request['type']
            ];

            $connection->Subject = $template->contact->title;

            $content = strtr($template->contact->content, $variables);

            $connection->Body = $content;
            $connection->AltBody = strip_tags(str_replace('<br>', "\n", $content));

            if (!$connection->send()) {
                $message = 'Mail action failed!';
                $error = true;
            } else {
                $message = 'Message has been sent!';
            }
        } else {
            $message = 'Incorrect recaptcha!';
            $error = true;
        }
    }
?>

<section class="pages">
    <div class="container">
        <div class="col-md-6 col-xs-12">
            <?php
                if ($message !== '') {
                    if ($error) {
                        ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $message; ?>
                        </div>
                    <?php
                    } else {
                        ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $message; ?>
                            </div>
                        <?php
                    }
                }
            ?>
            <form id="contact-form" class="form" action="" method="post" role="form">
                <div class="form-group col-md-6 col-xs-12">
                    <label class="form-label" for="name">First name:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your first name"
                           tabindex="1" required>
                </div>
                <div class="form-group col-md-6 col-xs-12">
                    <label class="form-label" for="email">E-mail:</label>
                    <input type="email" class="form-control" id="mail" name="mail" placeholder="Your e-mail address"
                           tabindex="2" required>
                </div>
                <div class="form-group col-xs-12">
                    <label class="form-label" for="type">Type of message:</label>
                        <select name="type">
                            <option value="Ask me">Ask me</option>
                            <option value="Problem">Problem</option>
                            <option value="Website">Website</option>
                        </select>
                </div>
                <div class="form-group col-xs-12">
                    <label class="form-label" for="subject">Subject</label>
                    <input type="text" class="form-control" id="subject" name="subject"
                           placeholder="Message subject" tabindex="3">
                </div>
                <div class="form-group col-xs-12">
                    <label class="form-label" for="message">Message</label>
                    <textarea rows="5" cols="50" name="content" class="form-control" id="message"
                              placeholder="Message" tabindex="4" required></textarea>
                </div>
                <div class="text-center col-xs-12">
                    <?php echo $captcha->html(); ?>
                    <button type="submit" name="contact" class="img-rounded">Send</button>
                </div>
            </form>
        </div>
        <address class="col-md-6 col-xs-12">
            <div class="contact-logo col-md-6 col-xs-12 no-padding no-margin clearfix">
                <a rel="nofollow" hreflang="en" href="<?php echo $siteURI; ?>"><div class="logo pull-left"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfgAAABRCAYAAAA+TLTtAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAigklEQVR42u1d34st2VX+9rknNxiThx7MYEyiTE/wTVDPHYiIIKQv5VNIzPTNk+CDuVdEcKIy3eYPkO6ZhEyEIHPjgyBInJYk6tPhthBQhJBpI/Oc6WAwRhKZfojGzORWLR/2XnuvvWvXObuqTtWp070/ON3n1I/9o06d+vb69lprK0RQlk/eBvAR/aJfAdTPEHBbeUcRACX+98J/A/hPgL4K4Mu3bn3/q30LJHrm1wF8FMCHADwF4B19y1yB/wHwHwC+AeDLAP5Oqa+/NWB9GRkZGRkZK1Fj5vLxkx8F6AVAfUBviRE4BafHyJ4ApQBaMQBQaNhPXwPwu7fm3/+3th0ieuYXQfhzAB+st3PNlaDY8U1l0Kpt3wRwpGZf/1Lb9mdkZGRk3EgsAOybVwyXAK4AXJj/a2EZqvzxu28B6k8BPK+3hARG9gTi7YoA8klegdz+GvFTvBW1ehQA+iGgfvvW2753lnp1qHzmEKC/BNQ7mutc1R6zTcH0iyLnrbma/iEvAPiUuvVqmdqHgXEE4CDhuHMApxuu+2U037gSDwEkf+dbwgLASctz7iHxR9kDjzZUziX8h8lFz/JOoK/ZVHAGfZ+tQ+r1vLuFPozRtk3dTyk4xvr7bFfvo1W4D/1MPuxw7jn0Nbs07y/DA+b2HUGQu9voyFezF9m3Prkzt5F3noJSZIx0ipCmqMeSqiXgdwD0N+Vb77516/b3v7iup/T4zsdB9NcAZq6OhroaiZrEP4rva26BuB4WfD2P1n5V4+ASaQS/j80S/B70jZyCB2NflA44RNp1DM/p+zBYh7ZtSsUV9MPsDPpB0haLAdvWBal9mFKbt9G2Mfu/l3DMrt5HMdyH5oUUo6cJB8H1YKI/Ne8xA4DyzSefBannNfcRFJHjQfveEBgpoOL3bj+REucpt70y5VTmWP5stwX77TGkxw2kvlC++e6nVvWSfnznKRD9BQgzWwaJ8mR/bNvD4+AfVzs3LIfLktcG9XIqPE9v3Xm2/720EZwhzYrcx2ZHyqmjUx6NTh1dRttTejC1BQ/QHpnXlKyojIxdwh6AV5CuaLbBPvTv1JY7K3/05G0QfVaTFZM1BFmhgeQoIDZyZG5IWjFZVzADA1OrNKItEaqgTsXnvhMVXljZLaJTEN4JOZiouG4V74dtE/nEz+d5AxDRDz6eO2DrIfHi42zFL9Gbd24Pd8+0Qqr83YXEmpBKblOX5gE3T9YWh0izUqaOA2iS3+T9kZFxE7CHkX87MxAdKsL7PEILLeuQ6ELyM8cqwCNvYkKHIT65n4I6JIkC4ngCgN8sf/hTPxvrAP1o8X5U+Jhv/SNO6jyosMdUwWCDxHby2+a1t3LbIc8X/z3FAO8F6N5YX+oapMpKm7oJ91qUtQsE3+e6XBdSZCskW/IZGel4GSP/ZmYg+gjVyFb/V+a9suRu5scleYkBAMltbOVKSdxawcELcNZubBBRYQaoD0d7QPRhEM3iSgPV21nJ7cq1kV8gRK13yDJlf0j0E8LCh7+too+M+cWuQBuZfhMSUpbnHXZZpo/hlW03ICNjR3CILQzwZ6jwjBJzyUoQJNn/cIRXmlcgz6uSzIBACVJTbrAQnds29VkiV95ctjxHVbgT7QHRHX8wUKFG9lYhkORduW3SSvfUi3DunkQ5YfnwyyrD+fyG9m8HY8r0WZ53uC4yPYPn/DIyMlajbdTNRjAD4adJzEVTKHHHJGpr5foETZ51DDsIUBUb6HKf2G7qloMLRbADDJACEb0n2oMK7/EtbTGPbufBZbvZ4ga8+fRwHr3mb6ACZQLBgCEYAHhlAqga2r8djCXTZ3l+mDKmhEzwGRmrcYj2hsEF9HM6fKWorxZzVHi705Thz1XbbcBs9n79XkaCcTi8qu8Kj4vuU4DCm6joe6ZqVzGJRujY+oZMdISf9LwCKajclflr6l3f+GcAoB/80msAfgGAJl99+M+pd33j2y2/BJjyRHNJF0i1izgVJzvAyfTrrEm2WLtK51mer+MAw4fLrcNxw/Z90742D6MF9H206sHTJY6ey10Hjtlvg1YPyYy16BMuxkj5Tnb1Pkqdd7+C/m2um0blKCcOk2v8vc7Zco6GqBuu0u8Jw0CoA5FdSsV3WfD8eJicRgWWuwpIn5Tp+Ca6wBdKkLuXGQ9+/dPAGdKsr0N0j4lPvbF3wXrvK88zWKbfJsms+z6P0E5SXGD1Q/44tSCBR0ib3nmY0J+MYTFWop9dvY9S6r8EcAdpzwUejPBzcwH9LK9NAc5YJlcx5zfpbDfSy9WlydHO/1eIgz3hK/GeqkBqd+UBcNu9sLoeX58vxcenNqo+FQyCMWT6mybPp5L21GX6U7R7mGZv+oyMZqT8Pk7RfdB/AZ0g7Gno360tZ8YESjVCAlRF/jz5IC948+8kPdxLAkq4BDox2Pl349QXxr9L8mXIQYScb+8KIr+sWDz8UAJId6R60w8d932d5PkzpMmIu+BNn63ijIzxsAkj5wr6d2ufQTPfOxyaVCudzY7Y0a0WW77JF0FVJklOqQyxIxhsKN8Cl+A2kz9g8EkWPoFXZGLZTR0lNZefAnl9Ym0p0U8hGA5DetPfRO/588T+7Io3/SbmVjMybjLazL9vHDMlyTaW4Ca0dgew4MmoBQD8wYYhajvIiCHmye5J8NKz3UAMLrzzu6KSUwLyOgZW/fQwZE7umyjPc772TZWZkZFxMzDI82CGUsx7G+JTJWpW9nAEbyx4MiQv22EGFjbGPgYpwYftDMneOwf1ufKuqCKhebHUtdNDqkx/gHYW502U53mwlOqNuwsyfSp24TvMyNgGUj3/jzCAqjez8eulIyPp2KYMAQ8m0VcAmRhzYkI37aHSzM1XqnmK3JJ7hZrTnOcwKBieCbgk3yGvK/wFcupz76EPwLQwhMV5U+X5Nv3aFZk+BZngMzL6YYEB8tTPSKSk5cVWrOVsSRfDWfBmDp4HE9oaFtF5lTISfQMBswRfQpA21eVxSbAlBWSMnnPwog08UAqJfpoWPDCMTH+dCL7LVMN1kelT5g+v0H+9+IyM64zUZ+wCOv3zG9B5672V4bpgjopEuLsmIbLx4WS3qsHcwI2DnVipxdbE7VAurL0GuaCNKwJBap2wynrsep/+eeRN8NeFnyyxM1KT3qTGb6davNdNnpfX5QJp13QKSW+akCoZ7sIgLWNY9H3I3cX1dui8QDsDiZdnZlxBX58LuAx3SZhJpzAyirUqw9h3isvem3qRq0+mvlXsfV5ijURPkbYGsfAyDp1XgwuXeu0KrxzZpuC6TRebtDivk3NdF3m+Tf+mKtMfQhN8CnI4XUbGavQdxHPK7xNoGZ+gLf37WPP8mMUI3GV+JUG6wzra2Rz4HKZnHe+AlRK61zb4ROuF/olzomTc4/JbJ7sGZ7/pOtkxNinTXyeC79OXsZflbYODyOvIvF6FfnikDDxOsRsqTEbGNiGzzm0Kh9Ay/utYobbNrWOaSNuqs6rqld0Up4BVQxEUQVVKq9omL7yCnnvXdSqd6KYJcnlWkVjeS1lL8BV7TlXrsXqPVLJ2qVsj9XOaWk/5nzTBb0qmv4nyfFNfUn/Q25DpH22gjAt0Sx2akXETcYz20Ugp2IO27I9MHd6zZKZ4WdMwBr5k5zfSXvaDSvRCSi/NNAGxkx+tTjRnreQKUXlcyPwW7JDnhcv1IGCTcc965NtrGkj308YmZPrrZL2nDlbOOu5jTFWmX9fnsfKPZ2RcB1xC/2ZSwpK7YA/aon9ZbpzrpVlVbV0ZuXaLIgCzAS1Q49BHULoNweeVNZeBg1vMag4LqYTn3ia6FTrZeRVO2nKXOEfa4jOrPKs3TfCp88BNaOWQ0rEv52v2pfotTNXZTuISWpbfhbZmZEwNF9Ak/wjDDervw61Kh7nNQa90PLpSZo13pYwqT3r7gF70IBJe/KYu0xZd6wr5vAoJNQIVLN9KMPF/G/J29wie20K9nfNHRhuZ/kFk+xDyfJsVzWI4xrAEf4nVIWJnCEbUDZiyN73sy71tNyIjY8dxAb0ozAnSDKouOIJZP37GTnTKyOLESWWMNO+SzQz3opLj7ZXvKFfCSuyqSUJneZ4COVzK/2UVxMFLOb9yHvdd0ZTSl6cC+k4BjIcUy5o9OkNcp9j3TcjzQHqM+C7I9IfQDnh55biMjH64glv97SGGke2PAHaygzM0FevzEAYo0bBOdiY8jxTVbHWtopMfWi5RhR+EBd1koVfB576okXegKmxqKmB4pMr0B6iT23Waf9+EPC/7m0KKuyDTc7atu8jJbTIc+jpb7oLD7RC4hCb6Y+jf/wL62doruY3BAYDFXEmyY295w6ZEgFLrMs30BVnCVQpCkifnAK/WpKoNy4PCSn08RdZvg2h5u8HoAbrK9PtII7Hr5D3PySfW4QxpUw27INMD+t7IJJ8hkXMh9MMV/N/+PgxBox/hH8xZQlbKzLJzyBrIRHvp+Ws1mJOdibU38+QyyI2j97gtUchMdkyy7kSDgHw5N72Sx/cIk6tiigG3ydS/O3x/hvVWPMv0bI1fJ+t9U/I8gxefWVdmaqbATSC0uPj7TH2QsMfunRHampFx03CJOuEfon3q2sXMzn+XetU2EvPHOqsdBVnthniZeks9Hw+u07TBJt6JIUwmY8PV+HMkkY0NoQuS43SFN/9eiTrFPPxuzMED3ZLeXCeCT+3LfaQvqZT6oxwr6c1p8DqGng+8h/QBxgL9oxwyMjLWg6NXnka76ZC9mXawc4SkglS1nL528Cx2QcpaGQuveBASQ0k+ocqFZ2R2PBlOx6vIEfkrynWFJXMxwLCDB9Gu3UDqErJMRjdRnh8K215Ctm18+yBLXGZkZDSCB+QpWMzIJmVRkOuak7BylSWvgV6k1QNrsRtSt0lqyHyOwXrKU2Cxk0/+Xpgce+gLK7vsQcDeKnJM8mGynZ0heKCdN/11st5T5fmhMAVv+jYZ6sJFMTIyMoZHqq/O3kxVyhGpkLp5u6rUwBa8WFSmIqEmwA04mDxjCCXx2CIvXAZDyvbynK7gULhwoFFiM2F446PN8oZDhsepnq+2zj9TWL51Cm1ok2M+y/QZGeMiNfwWM0l0bD3zXDKT7fBz8LCWrl2f3pvDRnOqV+98Ct6TSIMbnEMw8fNryk9BRTq2PjJQkn3bIbSR6VMIPsvz6di2TM9IHRxlKz4jYzWOsHllMEXpO59JpzonxQu53iOt4ax4681vZXV4L2qygEtyFnKFiJNbVZfgK1FHWWEjFrx0pqslvDFW/G4hxeJOvWmzPJ+OKcj0QLsEHNmKz8hoxgHWrPrWEvtIfFbNVGh1kvLn4Ckkr4Fe4QIwbIUTGau+IYyNIuTKUnkZfGZ4Ej3qEn5b2Ln3wA9AtmG3LHige4rXGHaB4KdgvU+tLalWPMftZmRkNOMEwBvQIaZ9MkKmDqgv5laiNlBeqhkT2a16EuBKkPunOLkOjPu+36YomDhj2eLEErR+lXwsbWYpXJvoBnUlIFi9doeQmvRmHbI83x5TSXrzEOnrAXD+65uITSkYOWHMzcB987qAfs6eIe0ZyfknUp9Vl3NH3DIZjGYlm/ulwrCZ7ETRHhfbrHoENKW6sYTKxwSFcY5bFVjwsv4+a8Hb8vxrJzMC7iK7G6QkvUkpY+pos1BO3+QuKTfDmElvVuEKmuRT0xfvYzcGc5tG30WRGJngbxYW5nUCt3AVO8/x/z24UOS2RsjZHJXM3W5gOa+JeTcMY00TryRXz0jffG5tqVbZAUKUwL1c9BtIWetl04vt2K1l5QRSc9Ovwi4Q/CZzz6/DWWJ9U8lNf4r0e+AI8ZUGMzIyVoPn1TelJJ4BuJr5jnTKxXDXHN0GnoOvlHHygzefTvKYGMLY96YsddLJLZZlrreTnXDy88LkRFt2D6ne9E24bvL8JgYrXTIFbhOXLdp8H9NwEMzIuOl4CAAzLwzNLm0abBua4EMHO5s4BjYBTqMPQMmhcKgTvSTwMjgndLDrm+jGtoGvo2gH9Sx/u+hDartgvafK8+vWfk9F6jWZijc90E5JyCFzGRnbxSnMoHwWzfrGJGXJFQNmsoMjQ8+i5gQ8vEb8ijl4G4oGwMuAJ0Lmwkx2bNWLbHmdUXHdleiLHGj0VAi2iz6y9C4Q/NiZ+K6QPlCYiuNfqhMQkEPmMjK2iQsIX45ZLYlNLFyNgs8bXmjGpaUNZfXgFQPJcyv/vx2oIAiTA/z4/kqTc1fIePqwL7HFbnYLXWX66ybPbyNscCoyPZAT32RkdMVYzrIX0GtJ2Ppm1EC6cdIf4iVi7iNZ87zPMZTCepdz4J6FXvnnN+Wr74rawKTyFYq+q9VtH12s112w3lPl+Stsh+CnJNO3GehlKz4jw+EedPTNKYYh+yvo9SPuhOXPaquesTxeI9iB5+DL0KkObvnalbnog/ZLQi2D/wyZlEaqE10R6w+ZeqRkv7voQm67QPDbWiinzXz+VGT6K6TPxefENxkZPngRpyegifgY/Y2GS7ilnqMK29wLGbPT3KQ/EFw895Bx8Fw5kYt7VwSS9TZVz46BNuad/Cw9sfNK0kH+Mr6/T/dKqqcR8AYMQ8YYjoKz69CJCI7Rbn3lTaJvPH0MQ39HY16vNsvWDoUp3/NjtG3K/U/FFO6jEBzvzqTMOST2oFXFVardOZyiuHYKdO5leiNYYlREmi95ADAgwevEdTzIMJ9tvVidi6aEIFTTzgqmE3Cp8WTzvbLNAKHPvVwFBF8bWZDIGpSRkZGRkWExWAbIOVuampI0k1oqUib9zCqC3QA0ubsKCIaTSaepJdmmEI1z22K0EurvtXN6Dl6q8ALFyttdL7uMjIyMjN3D3Cc7R/Puk5PMh4EZQJhaeYhBpmbbJrUiTE422BUCj2gpco5XZB8nO/6jgvozMjIyMjK2g7mSC82IqXeAeYrEziGg5915WCGHGPJv44IzJfx59JrRTnXFvLTzEJvpAs/By0pkltqMjIyMjIyRMbdz3cyKlXJMb1db42OGAAnNwJC7rdM/Kn56JYjd6vrwnATDFeV43p1XoOs7Px46KoYOftyujIyMjIyMkTB3HuDkHNHldLHSc+RDGqLaoc8xowIZPgwHGRF4Wehkw1fo5Lz6GxN7Regn0VN9sMCEblfDywSfkZGRkTEe5qpiDjIyuXKSOAHOQ3xgglKVcfFTPPuuSXGtis6x7KGnPII2S673Vn9rOrcFKpYIIs57O7uQXEZGRkbGLmMOa3waFjLkx4ani/waUKI3HMszA4rIKe4QIXsxlK4c3U4EhCpi4xnecrHh+w7wygulesoEn5GRkZExOuZUAmzF6pw2JiiNLXuY/YPNIZMgcAUl62WVe1XdlfBmUxAha76roO8At+kwOdhpDotaXHxGRkZGRsZ40F70SqeI1VYz+7ErR4QD85OIkgPLBjZHDdTKKXhH6KFMHiN6eU7smI6owmxBYUx8jpvLyMjIyBgXc1TkE6gldQqy1A4cB2+c+aRfHeC2NRrxMXm88X/Qx9i+Lqi1IUboWafPyMjIyBgPOkxOpKqzkV218Pch4+DNFAE4Ra3erqPLTBa7Jh+ASKKetVb5pld286YFOFQvrCNb8BkZGRkZ48EluhEObS4u3KSaGTyVurLpank+XhmiXOvAX4UfZJxfg2zuWdzBhH8XxCx4Cgcb2YLPyMjIyBgPcyKWyEk4fpv5eG81t+ET3di880QiAz3ns1uRqtYcEV3kJfbeWxwmiInvgqgi0DA9kJGRkZGRMQJ0HDwQTaVORIL3hiMqZ+NSQNEkGxM/uWqwxEOuV03nbICIY0vuxmYOMjIyMjIyRsKcKs7zHkn2wnHcI80fyzXjuE16NTvVnIveC1ELk9awpa4iBB+T8juCM+F5RYpEApngMzIyMjJGhrHglU3IYmVxZSR6KWMPApOxrkbu0qJfMcggkz/fnswx+4oL03XIuXq5HjzQf/qBZHYgiIGRqC/72GVkZGRkjIg5SKSDZTKUq6zaDHPDriYXrh2nlLN+V9ZvE/W44vziI+fZPPHi+D4EbPPhh9MDwtFu0577GTcSRVGcAFiYj8fL5fKi5fkHAA5EGRfL5fJ42/3aBoqiWADYE5sul8vl5bbbNUUURfGI3y+Xy7vbbk9GGubWsg2MZOtRDwwvMRP5BrdMdlM5lT1+Lh8fHlAFn1XDLrkoTEdwutxwvV005aifBoqieBXuQQ8A95bL5dkW23MtHyKmXwfm493lcnneo7iFKGuvzYlFURwCeKVnPwDs9vdTFMU+gJfhrqPcdwn9O2g1cLoBOOhfRMbYsAQv12tRcHSorEU/pEQPK6uzLK9D58QUemOim1QnuRWpalc58SV1Qc6/74Z3nXnILYLNBwC2RvDID5GhcSjePwRwCeCqxfnX5ft5BfV7n7GPlgOnjIypYk6VIVSlZWQ5F27lcrHK3OZBZjlapnXN5p4HP6nmfPRyWVa7sEuYKhZ1JzvrBKf6z4+TmGbw8s9X9eVjp4PDhm0Ptt2wjGb0tJz3RDk38ns2srydnoCe5jgX2w87F36NsVwusxfRDmLuwsCNBc3Eyp7r0qwfDMaVTvCuMsSryX/FanI8/+2t2iadCHhfINErYfn35V72oucl8GS5gw2MekM+yM7M572iKA7byvTm4XhiPl6Y1wm0NXS8XC5PW5zP21rLwUVR3Bf9svPTwfaz5XL5sEXfHomPx6adBwDOtyFTd5mDF9d3IbZxv9Zejw1+P3umnEP4VvI5gIcjTQ/Jes94usRcxwtoZaM1Gvp2AeC0w+/Ju19NeUfm/9opHuNncST6+FDsOwRw33xMvuZ9pmfMtbkv+gBo5eh03bMhUtbClBM+v047+KPEyrqC/t4eXAd/DLsevKo0szo/O2O/k5TQhwBZhzo/ax3Jqfn1Ev2qzLCx5WGFY19/ghcqAAWDC/4/ofFvIM/zQ41v8i4y/R6cfLsP93Dpcj6jixwsy9lr2N52/lu24xXTv22iyxz8quubcj029f2cwJFLWFYfv4SuuF8UxRX0YK3zw9wQ2CPUZf8FgFeKonjQZlCJ+u+p7T13Af8ekXUfin1tVJxO0zMrrg33MZngDSE/Qv2+PwRwUBTF3ZYkH/s9c7v2oaewdhozVNBLtBLZUHLepir9nthKHurl1WFC2rhdFQkv9QiI3HnyfQVN/qU4hlGR21e5NnRGVQFlZcqFexFcHdPyovdGv8YiuIrs64J9U9axeaXM8V6aYyWOxWsq2Id+IB1jO4TUFXx95QOLr23KA3FT348cVDxhZN87SL9PNoELURc7271eFMXrRVG8bAa/bXEfjsAeAngCwNNw1/ukQ5mMfdPmY+h7b+11Wi6XV3CD9AX3yZCtVLLGIDB5bS4B3ANwF3pw0daR8QSO3I/N/cNqAqscSTDXhL/r0+VyqUR5Sdd5FzAH0Q9A6l1KmJns7sbSuc4LX8LZ9S5enniOXsapyfntiPkaj0wLPeEDdz9quOAVrmrWuec0Z50E/4k+ddtMORC8uXv95t/pU7cRdYzjcxDZbdax98r0T+Y2vDXGF5oISeJMVGfQP8ZOMn2AO20eHubYUyM/87ZW0t1IaGuJTQLi+rJl0ur6DvD97EFbXBdCGh/rWlwVRXEXdUtwH/r+P+xgCd4X5bNVfFUUxUMYUiqK4qBj9MTlcrm80+E8nnYD9MBKqnTAeANUqdg86BpBYgjZDhD5/jP+E+dmX1fjZN/8Ni5M+3Zp8L4SMyJ8F5UhV2PpKrZ47XY4y5797XjuvpIWtOG4ioBKaetbWteVs7IVW7Xmv85P444n0x5lP+O/oj0g+i5ChaFy7fH3EVBSsF9a/tBSgi3HbC8rV5Y9XpxfkV+H1xb7+bvb/rKBmjx/KR5k8qbu4y3dS+6cMnaR3CcGvscW0PLo60VRvGEs59E81809/zS0FcnRBIxWlqCBtfqLoiB+wbfcFy3LZHS658wAnY2ig+A/MF60jL02PcNDpbJyEFxn2y9D1CnX5xLuez+EHvC9YZSctt//ZDFXFV4D1M8DsOFwznYXc/AMa5AL2dtYwS4pjt5v16mBdpojRVr6563BPLln0YtEN/pQei3aA8Jrti2k/HJlQhtdqL9dCTJuqltK66HU7/VBevBz+Uqco14d5ytdCznK3RPOM3vBMTfSyzpjULCcLx3R2AFrAS3XjwIjY4fOZ5wj4Lp40rMqdxiR56+FBN0T91B3stsHcFIUxd51SAA1B+ErBHrWWuUqyPtuiM4T2j2Sc4QmHdPl+UzoyiNAZT3kY7Bl6ZXfKgB/Hz2won8A1J8BmIUx9XXYTq44TgwK5JTBujC98HwAbnF7BaD6SudvabPwCB5xa30TMv22sYCzGLftGJcBS6oPADwQStKR+d/Vwm0Fno8OVablcnlWFEXXYq+gf0tXy+XyiTH6kYhzOIn8ZbF9zN81XxsURbHfQ92TA5Kz5XJ5r2/DjJJzz7SN70Ge5x/lfhwacxDOFOEEwPv0JmfSakNcufe825sqF3PT4QF2sp1EvLkbBbic82vr+JJ6sfp2rAPqhcffpj9+25cAPFu3qNmiV/42FSFlHnwo5cLopBs/z7OHi8isWk/eqRnfwXYTyACoyfNXqHuw8jwksJ2kNzyXxhbVFfRDM3U+VD4EDouiuAj6NAUcmoeJ1+4dkf97fT9G+jxfLpcXLJGachZm/94IluU+gEdFUZyZ/kiZltHWJ0D6rxxJ/wQOS9tGSKUZtFyaPnP/rkYeuPO1AXREwb3lcnlpnkWHqb4cy+XyQvZF+jQIdWLRJr+DuR/PlsvlpSifQ/muRbKjuXqxfIv+6NYfQuEVO4cs2VUF24QRrKV3cQwpP7ELx9ATn2hd9mThvg3NhVY2cc3/AmrNnAg9D+A3ALzTXyBOWPRyasHzmK/8AUDoTS9Xq1PBfoJx9Ycj+7ih/0n16cdvjvOVrkToPe/9uES8Kh87tkwvR/csl57Decqug5zj45AaQFgRE0BssHGOjnOtI6Pv93MALX/yeTIX/OXIsvEhmqX4tg6Ep3DTDieGOGSo2jZxBt+nYOxBu7w2C2i/C953jnbX+hjuvntkCPkKzmhpO8d/Av19XaH+fV2LVMUzAFCfKc9AeBGAcLaTjnEQzmrOuUyxw1m4j53kKmEZk34pdkrznOzghcJZZbsCAfgd9ZlypayjPv34WyD6BCojJ1BYh2hnJUnfDEqkMyBs3S6BDZ9TKXEcl6l8xz7e7hz+XlSffrx1691gpRetecDyjb1nrI8xcYweDyBjFYaDkofYDfLcBfT6fuA/NA/gyP0cRiodAZz3IfZMuYIOv2rVR3Pf3YX7TcmpL2+ufwsI+zKqh7i5Nnca6m0l15vv5Z44z3MY7tA3+6yDT+4PMa3w3M5wgvYnZ7cAvATg971dMv2rly3OHeNUbxLbqV5NJJUsWZvemMFOMXgTwCfUZ8u/Su0MffLWb0HhCyC83a87bEsYBueqj8bwUfy0lanm9b7PA+oP1Gcfl8gYDUaJWCCvDjZJBKu4be07EvcJ0G4qKLXMfP8JbPLaBHHsF33Un8DzvldZU0PNE42em30MWrr4QPxwWl1UdOGWMB5e7GcZ3y/3awB+T71U/WvbDtFzt34ZwOcBfLA2me81v8kRT+6LnF9raqws+iag/kS9VP5t2/ZnZGRkZGRsAlGGo+du3QbRIYCPAmoB0Huh1NsccYfhaGJbWHqC9UvA/ymobwH0LwC+qD5X/WPfjtFzsw+B8HEAvwqFp0DqJ1ybY4l4Yg0VqoXdFTlW4ccg9R0AFwB9GcCZ+lw1pcQ2GRkZGRk3DP8PTUNwnhIqkrMAAAAASUVORK5CYII=" class="img-responsive" alt="V-ART - ArturLifts Services"></div></a>
            </div>
            <p class="clearfix"><i class="fa fa-user text-center"></i><b>Artur Gladosz</b></p>
            <p class="clearfix"><i class="fa fa-phone text-center"></i> 0044 (0) <b>7955-656-761</b></p>
            <p class="clearfix"><i class="fa fa-phone text-center"></i> 01707 <b>396589</b></p>
            <p class="clearfix"><i class="fa fa-envelope text-center"></i><a href="mailto:artur@v-artlifts.com"> <b>artur@v-artlifts.com</b></a></p>
            <p class="clearfix"><i class="fa fa-map-marker text-center"></i> Welwyn Garden City</p>
            <p class="clearfix"><i class="fa fa-clock-o text-center"></i> Open: <sb>8:00 - 17:00</sb></p>
        </address>
    </div>
</section>


<?php include 'footer.php'; ?>


