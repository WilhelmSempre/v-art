(function() {
    'use strict';

    var Vart = Vart || {};

    Vart = function() {
        var self = this;
        this.phoneUs = function() {
            $('.phone-us-form').attr('novalidate', true)
                .on('submit', function(event) {
                    event = event || window.event;

                    var self = $(this),
                        message = self.find('.message'),
                        messageClone = message.clone(),
                        phoneInput = self.find('input.phone'),
                        phoneValue = phoneInput.val(),

                        phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

                    if (phoneValue === '') {
                        message.remove();
                        self.prepend(messageClone);
                        messageClone.show()
                            .addClass('error')
                            .text('Fill required field!')
                            .addClass('animated fadeInDown');
                    } else if (!phoneRegex.exec(phoneValue)) {
                        message.remove();
                        self.prepend(messageClone);
                        messageClone.show()
                            .addClass('error')
                            .text('Incorrect phone!')
                            .addClass('animated fadeInDown');
                    } else {
                        var ajaxFile = $(this).attr('action'),
                            ajaxMethod = $(this).attr('method'),
                            ajaxData = $(this).serialize();

                        self.stop(true, true).animate({
                            'opacity' : 0.5
                        }, 200).
                            waitMe({
                                effect : 'facebook',
                                text : '',
                                bg : 'transparent',
                                color : 'rgba(255, 255, 255, 1)',
                                maxSize : '',
                                source : ''
                            });

                        $.when($.ajax({
                            'url' : ajaxFile,
                            'type' : ajaxMethod,
                            'data' : ajaxData + '&ajax=1'
                        })).done(function(response) {
                            var ajaxResponse = JSON.parse(response);
                            $.event.trigger({
                                type : 'answer',
                                error : ajaxResponse.error,
                                message : ajaxResponse.message
                            });
                        });
                    }

                    $(document).on('answer', function(event) {

                        self.stop(true, true).animate({
                            'opacity' : 1
                        }, 200)
                            .waitMe('hide');

                        phoneInput.val('');

                        message.remove();
                        self.prepend(messageClone);
                        messageClone.show();

                        if (event.error) {
                            messageClone.addClass('error');
                        } else {
                            messageClone.addClass('success');
                        }

                        messageClone.text(event.message);
                        messageClone.addClass('animated fadeInDown');
                    });

                    if (!event.defaultPrevented) {
                        event.preventDefault();
                    }
                });
            return this;
        };
        this.scroll = function() {
            $('.scroll').off('click').on('click', function(event) {
                event = event || window.event;

                var href = $(this).attr('data-href') || 0,
                    duration = $(this).attr('data-duration') || 500,
                    menuHeight = parseInt($('header').outerHeight(false));

                $('body').scrollTo(href, duration, {
                    'offset' : menuHeight - 2 * menuHeight
                });

                if (!event.defaultPrevented) {
                    event.preventDefault();
                }
            });

            return this;
        };
        this.hamburger = function() {
            var navigation = $('header nav'),
                sticky = $('.sticky-wrapper'),
                navigationHeight = parseInt(sticky.height());

            navigation.css({ 'display' : 'block' });

            $('.hamburger').addClass('is-active')
                .off('click')
                .on('click', function(event) {
                event = event || window.event;

                $(this).toggleClass('is-active');

                if (!navigation.is(':visible')) {
                    navigation.slideDown(200);
                    sticky.animate({ height: navigationHeight }, 200);
                } else {
                    navigation.slideUp(200);
                    sticky.animate({ height: '78px' }, 200);
                }

                if (!event.defaultPrevented) {
                    event.preventDefault();
                }
            });

            return this;
        };
        this.carousel = function() {
            var carousel = $('.owl-carousel');
            carousel.owlCarousel({
                items : 1,
                responsive: false,
                navigation: true,
                navigationText: [
                    "<i class='fa fa-chevron-left'></i>",
                    "<i class='fa fa-chevron-right'></i>"
                ]
            });

            return this;
        };
        this.resetCarousel = function() {
            var carousel = $('.owl-carousel');
            carousel.data('owlCarousel').updateVars();
        };
        this.init = function() {
            $('.sticky').sticky({ topSpacing:0 });
            $('select').selectric();

            this.scroll()
                .phoneUs()
                .hamburger()
                .carousel();

            $(window).resize(function() {
                $('.sticky').sticky('update');
                self.hamburger()
                    .resetCarousel();
            });

            (function(i,s,o,g,r,a,m){
                i.GoogleAnalyticsObject=r;i[r]=i[r]|| function(){
                    (i[r].q=i[r].q||[]).push(arguments);
                };
                i[r].l=1*new Date();a=s.createElement(o);
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-80264457-1', 'auto');
            ga('send', 'pageview');
        };
    };

    $(document).ready(function() {
        var vartInstance = new Vart();
        vartInstance.init();
    });
})();