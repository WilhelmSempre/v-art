<footer>
    <div class="container">
        <div class="col-xs-12 topFooter no-margin no-padding">
            <nav class="col-md-6 col-xs-12 no-margin no-padding">
                <ul class="pull-left list-unstyled list-inline">
                    <li><a title="Home" hreflang="en" class="text-uppercase" href="<?php echo $siteURI; ?>">home</a></li>
                    <li><a title="Certificates" hreflang="en" class="text-uppercase" href="<?php echo $siteURI; ?>/certificates">certificates</a></li>
                    <li><a title="About us" hreflang="en" class="text-uppercase" href="<?php echo $siteURI; ?>/aboutus">about us</a></li>
                    <li><a title="Contact" hreflang="en" class="text-uppercase" href="<?php echo $siteURI; ?>/contact">contact</a></li>
                </ul>
            </nav>
            <div class="col-md-6 col-xs-12 no-margin no-padding">
                <nav class="footerlinksocialmedia">
                    <ul class="pull-right list-inline list-unstyled text-right">
                        <li><a title="Facebook" hreflang="en" class="text-uppercase" href="#">facebook</a></li>
                        <li><a title="Google+" hreflang="en" class="text-uppercase" href="#">google+</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>

<aside>
    <div class="container">
        <div class="col-xs-12 text-center copyright">
            <p>©V-ART 2016. All copyrights are reserved.</p>
        </div>
    </div>
</aside>
<script src="<?php echo $siteURI; ?>/assets/js/dist/main.min.js"></script>
</body>
</html>
<?php ob_end_flush(); ?>