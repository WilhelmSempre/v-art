<?php require_once 'header.php'; ?>

<section class="subpageHeader">
    <div class="pattern"></div>
    <div class="container">
        <div class="col-md-12 textSliders">
            <h1 class="text-uppercase text-center">certificates</h1>
        </div>
    </div>
</section>
<section class="pages">
    <div class="container">
        <p>We are a company with a lot of certificates. Below are just some: </p>
        <div class="certificates">
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>NVQ4 Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/1.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/1.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>FdS Lift Engineering Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/3.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/3.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>Unit 15 FoLT Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/4.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/4.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>ENG2024 Advanced Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/5.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/5.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>ENG1028 Contract Management Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/6.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/6.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>Electronic Systems and Control for Lifts Certificate<</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/7.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/7.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>Advanced Lift Technology Electricial Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/8.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/8.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>LEIA Certificate</strong></p>
                <embed class="certificate"  height="300" src="<?php echo $siteURI; ?>/pdf/9.pdf" type="application/pdf">
                <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/9.pdf">View</a>
            </div>
            <div class="col-md-6 col-xs-12 item">
                <p class="pull-left certificate-name"><strong>HFT4 Advanced Lift Technology Certificate</strong></p>
                <embed class="certificate" height="300" src="<?php echo $siteURI; ?>/pdf/2.pdf" type="application/pdf">
                    <a rel="nofollow" class="pull-left view clearfix" href="<?php echo $siteURI; ?>/pdf/2.pdf">View</a>
            </div>
        </div>
    </div>
</section>

<?php require_once 'footer.php'; ?>


