<?php require_once 'header.php'; ?>
<section class="subpageHeader">
    <div class="pattern"></div>
    <div class="container">
        <div class="col-md-12 textSliders">
            <h1 class="text-uppercase text-center">about us</h1>
        </div>
    </div>
</section>
<section class="pages">
    <div class="container">
        <p class="text-justify">
            Nowadays the lift industry has become more sophisticated than ever, try to
            achieve best service in the vertical transportation. Mostly, people think, the
            lift is a big piece of the steel which is moving hectically through floors not
            necessarily meeting expectation of these people. The lifts combine the
            mechanical, the electrical and the hydraulic parts. It is living in perfect
            harmony until the breakdown will come to ruin your perfect planned Friday.
            Our company was established in 1998 at continent to provide best possible lift
            service for customers European countries and in 2006 has been moved to UK to
            prevent that such of the occurrence. The good lift services are hard to achieved,
            no one questions that. For this reason, our company keeps collecting and
            recycling all new solutions in the lift industry to be able to keep you going
            whatever your destination is. Our acquired knowledge, base on the years of the
            experience of the work throughout the whole Europe is the best guarantee of
            piece of your mind.
        </p>
    </div>
</section>
<?php require_once 'footer.php'; ?>
