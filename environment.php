<?php

$envTable = ['prod', 'dev', 'test'];
$env = 'dev';
$siteURI = '';
$scheme = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

if (in_array($env, $envTable)) {
    switch ($env) {
        case 'prod' :
            $siteURI = $scheme . $_SERVER['HTTP_HOST'];
            break;
        case 'dev' :
            $siteURI = $scheme . $_SERVER['HTTP_HOST'] . '/v-art';
            break;
        case 'test' :
            $siteURI = $scheme . $_SERVER['HTTP_HOST'] . '/vdemo';
            break;
    }
} else {
    die('Specify environment!');
}
